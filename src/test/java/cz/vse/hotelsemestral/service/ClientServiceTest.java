package cz.vse.hotelsemestral.service;

import cz.vse.hotelsemestral.entity.Client;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientServiceTest {

    @Autowired
    private ClientService clientService;

    @Test
    public void getAllClients() {
        Client client = new Client("test@test.cz", "Test test",
                new GregorianCalendar(1995, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 999");
        Client client1 = new Client("test1@test.cz", "Test1 test1",
                new GregorianCalendar(1996, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 777");
        List <Client> list = new ArrayList<>();
        list.add(client);
        list.add(client1);
        clientService.createClient(client);
        clientService.createClient(client1);
        List <Client> listFromService = clientService.getAllClients();
        for (int i = 0 ; i < list.size() ; i++) {
            assertEquals(list.get(i).getDateOfBirth(),listFromService.get(i).getDateOfBirth());
            assertEquals(list.get(i).getEmail(),listFromService.get(i).getEmail());
            assertEquals(list.get(i).getName(),listFromService.get(i).getName());
            assertEquals(list.get(i).getNumber(),listFromService.get(i).getNumber());
        }
    }

    @Test
    public void deleteClient() {
        Client client = new Client("test@test.cz", "Test test",
                new GregorianCalendar(1995, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 999");
        Client client1 = new Client("test1@test.cz", "Test1 test1",
                new GregorianCalendar(1996, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 777");
        List <Client> list = new ArrayList<>();
        list.add(client);
        clientService.createClient(client);
        clientService.createClient(client1);
        clientService.deleteClient(2);
        List <Client> listFromService = clientService.getAllClients();
        for (int i = 0 ; i < list.size() ; i++) {
            assertEquals(list.get(i).getDateOfBirth(),listFromService.get(i).getDateOfBirth());
            assertEquals(list.get(i).getEmail(),listFromService.get(i).getEmail());
            assertEquals(list.get(i).getName(),listFromService.get(i).getName());
            assertEquals(list.get(i).getNumber(),listFromService.get(i).getNumber());
        }
    }

    @Test
    public void updateClient() {
        Client client = new Client("test@test.cz", "Test test",
                new GregorianCalendar(1995, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 999");
        Client client1 = new Client("test1@test.cz", "Test1 test1",
                new GregorianCalendar(1996, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 777");
        List <Client> list = new ArrayList<>();
        list.add(client);
        list.add(client1);
        clientService.createClient(client);
        clientService.createClient(client1);
        list.get(1).setName("Updated test");
        client.setName("Updated test");
        clientService.updateClient(client);
        List <Client> listFromService = clientService.getAllClients();
        for (int i = 0 ; i < list.size() ; i++) {
            assertEquals(list.get(i).getDateOfBirth(),listFromService.get(i).getDateOfBirth());
            assertEquals(list.get(i).getEmail(),listFromService.get(i).getEmail());
            assertEquals(list.get(i).getName(),listFromService.get(i).getName());
            assertEquals(list.get(i).getNumber(),listFromService.get(i).getNumber());
        }
    }

    @Test
    public void createClient() {
        Client client = new Client("test@test.cz", "Test test",
                new GregorianCalendar(1995, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 999");
        Client client1 = new Client("test1@test.cz", "Test1 test1",
                new GregorianCalendar(1996, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 777");
        List <Client> list = new ArrayList<>();
        list.add(client);
        list.add(client1);
        clientService.createClient(client);
        clientService.createClient(client1);
        List <Client> listFromService = clientService.getAllClients();
        for (int i = 0 ; i < list.size() ; i++) {
            assertEquals(list.get(i).getDateOfBirth(),listFromService.get(i).getDateOfBirth());
            assertEquals(list.get(i).getEmail(),listFromService.get(i).getEmail());
            assertEquals(list.get(i).getName(),listFromService.get(i).getName());
            assertEquals(list.get(i).getNumber(),listFromService.get(i).getNumber());
        }
    }

    @Test
    public void getClientByEmail() {
        Client client = new Client("test@test.cz", "Test test",
                new GregorianCalendar(1995, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 999");
        Client client1 = new Client("test1@test.cz", "Test1 test1",
                new GregorianCalendar(1996, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 777");
        List <Client> list = new ArrayList<>();
        list.add(client);
        list.add(client1);
        clientService.createClient(client);
        clientService.createClient(client1);
        List <Client> listFromService = new ArrayList<>();
        listFromService.add(clientService.getClientByEmail("test@test.cz"));
        listFromService.add(clientService.getClientByEmail("test1@test.cz"));
        for (int i = 0 ; i < list.size() ; i++) {
            assertEquals(list.get(i).getDateOfBirth(),listFromService.get(i).getDateOfBirth());
            assertEquals(list.get(i).getEmail(),listFromService.get(i).getEmail());
            assertEquals(list.get(i).getName(),listFromService.get(i).getName());
            assertEquals(list.get(i).getNumber(),listFromService.get(i).getNumber());
        }
    }

    @Test
    public void findById() {
        Client client = new Client("test@test.cz", "Test test",
                new GregorianCalendar(1995, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 999");
        Client client1 = new Client("test1@test.cz", "Test1 test1",
                new GregorianCalendar(1996, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 777");
        List <Client> list = new ArrayList<>();
        list.add(client);
        list.add(client1);
        clientService.createClient(client);
        clientService.createClient(client1);
        List <Client> listFromService = new ArrayList<>();
        listFromService.add(clientService.findById(1));
        listFromService.add(clientService.findById(2));
        for (int i = 0 ; i < list.size() ; i++) {
            assertEquals(list.get(i).getDateOfBirth(),listFromService.get(i).getDateOfBirth());
            assertEquals(list.get(i).getEmail(),listFromService.get(i).getEmail());
            assertEquals(list.get(i).getName(),listFromService.get(i).getName());
            assertEquals(list.get(i).getNumber(),listFromService.get(i).getNumber());
        }
    }
}