package cz.vse.hotelsemestral.service;

import cz.vse.hotelsemestral.entity.Role;
import cz.vse.hotelsemestral.entity.Room;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomServiceTest {

    @Autowired
    RoomService roomService;

    @Test
    public void getRoomByName() {

        Room room = new Room(1, "test", 1000,null);
        roomService.createRoom(room);
        Room roomFromService = roomService.getRoomByName("test");
        assertEquals(roomFromService.getName(),room.getName());

    }
    @Test
    public void createRoom() {
        Room room = new Room(1, "test", 1000,null);
        roomService.createRoom(room);
        Room roomFromService = roomService.getRoomByName("test");
        assertEquals(roomFromService.getName(),room.getName());
    }

}