package cz.vse.hotelsemestral.service;

import cz.vse.hotelsemestral.entity.Client;
import cz.vse.hotelsemestral.entity.Role;
import cz.vse.hotelsemestral.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    UserService userService;

    @Autowired
    ClientService clientService;

    @Test
    public void findByUsername() {
        Client client = new Client("test@test.cz", "Test test",
                new GregorianCalendar(1995, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 999");
        User user = new User("password","test@test.cz",client,true, Role.USER);

        clientService.createClient(client);
        userService.createUser(user);

        User userFromService = userService.findByUsername("test@test.cz");
        assertEquals(user.getUsername(), userFromService.getUsername());
    }

    @Test
    public void createUser() {
        Client client = new Client("test@test.cz", "Test test",
                new GregorianCalendar(1995, Calendar.FEBRUARY, 11).getTime(), "+420 555 777 999");
        User user = new User("password","test@test.cz",client,true, Role.USER);

        clientService.createClient(client);
        userService.createUser(user);

        User userFromService = userService.findByUsername("test@test.cz");
        assertEquals(user.getUsername(), userFromService.getUsername());
    }
}