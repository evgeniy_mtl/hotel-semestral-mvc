/*insert into room (id, name, price) values (1, 'basic' , 750);
insert into room (id, name, price) values (2, 'modern' , 1000);
insert into room (id, name, price) values (3, 'glossy' , 1250);
insert into room (id, name, price) values (4, 'homely' , 1100);
insert into room (id, name, price) values (5, 'hutty' , 1400);
insert into room (id, name, price) values (6, 'vintage', 1700);
insert into client (id, email, name, date_Of_Birth, number) values
(1, 'jackBlack@gmail.com', 'Jack Black', TO_TIMESTAMP('1980-05-10', 'YYYY-MM-DD'), '+420 666 765 584');
insert into client (id, email, name, date_Of_Birth, number) values
(2, 'jhonGreen@gmail.com', 'Jhon Green', TO_TIMESTAMP('1985-11-20', 'YYYY-MM-DD'), '+420 666 321 555');
insert into client (id, email, name, date_Of_Birth, number) values
(3, 'ngedmond@sbcglobal.net', 'Howard Chapman', TO_TIMESTAMP('1990-06-03', 'YYYY-MM-DD'), '+420 666 528 588');
insert into client (id, email, name, date_Of_Birth, number) values
(4, 'duchamp@yahoo.ca', 'Finnegan Kim', TO_TIMESTAMP('1970-08-10', 'YYYY-MM-DD'), '+420 666 147 852');
insert into client (id, email, name, date_Of_Birth, number) values
(5, 'marioph@yahoo.ca', 'Leila Osborne', TO_TIMESTAMP('1983-05-05', 'YYYY-MM-DD'), '+420 666 365 452');
insert into client (id, email, name, date_Of_Birth, number) values
(6, 'mugwump@live.com', 'Aubrey Huynh', TO_TIMESTAMP('1988-01-17', 'YYYY-MM-DD'), '+420 666 765 325');
insert into client (id, email, name, date_Of_Birth, number) values
(7, 'leakin@comcast.net', 'Madelynn Gamble', TO_TIMESTAMP('1974-01-22', 'YYYY-MM-DD'), '+420 666 766 885');
insert into client (id, email, name, date_Of_Birth, number) values
(8, 'cgarcia@icloud.com', 'Sloane Francis', TO_TIMESTAMP('1981-06-12', 'YYYY-MM-DD'), '+420 666 254 884');
insert into client (id, email, name, date_Of_Birth, number) values
(9, 'kassiesa@live.com', 'Anabel Zhang', TO_TIMESTAMP('1982-05-11', 'YYYY-MM-DD'), '+420 666 321 123');
insert into client (id, email, name, date_Of_Birth, number) values
(10, 'tbmaddux@msn.com', 'Walker Jacobson', TO_TIMESTAMP('1983-05-13', 'YYYY-MM-DD'), '+420 666 765 798');
insert into client (id, email, name, date_Of_Birth, number) values
(11, 'gmcgath@me.com', 'Angie Douglas', TO_TIMESTAMP('1969-08-15', 'YYYY-MM-DD'), '+420 666 888 554');
insert into client (id, email, name, date_Of_Birth, number) values
(12, 'josephw@sbcglobal.net', 'Alec Parrish', TO_TIMESTAMP('1987-06-01', 'YYYY-MM-DD'), '+420 666 258 664');

insert into usr (id, password, username, active, role) values (1, 'admin', 'admin', true, 'ADMIN');
insert into usr (id, password, username, active, role, client_id) values (2, 'test', 'jackBlack@gmail.com', true, 'USER', 1);

insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(1, TO_TIMESTAMP('2020-03-01', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-05', 'YYYY-MM-DD'), 'ACCEPTED', 1, 1);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(2, TO_TIMESTAMP('2020-03-02', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-10', 'YYYY-MM-DD'), 'ACCEPTED', 2, 2);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(3, TO_TIMESTAMP('2020-03-01', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-12', 'YYYY-MM-DD'), 'ACCEPTED', 3, 3);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(4, TO_TIMESTAMP('2020-03-06', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-15', 'YYYY-MM-DD'), 'ACCEPTED', 4, 4);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(5, TO_TIMESTAMP('2020-03-01', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-08', 'YYYY-MM-DD'), 'ACCEPTED', 5, 5);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(6, TO_TIMESTAMP('2020-03-02', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-08', 'YYYY-MM-DD'), 'ACCEPTED', 6, 6);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(7, TO_TIMESTAMP('2020-03-08', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-13', 'YYYY-MM-DD'), 'ACCEPTED', 1, 7);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(8, TO_TIMESTAMP('2020-03-12', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-20', 'YYYY-MM-DD'), 'ACCEPTED', 2, 8);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(9, TO_TIMESTAMP('2020-03-13', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-15', 'YYYY-MM-DD'), 'ACCEPTED', 3, 9);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(10, TO_TIMESTAMP('2020-03-20', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-25', 'YYYY-MM-DD'), 'ACCEPTED', 4, 10);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(11, TO_TIMESTAMP('2020-03-16', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-22', 'YYYY-MM-DD'), 'ACCEPTED', 5, 11);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(12, TO_TIMESTAMP('2020-03-20', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-30', 'YYYY-MM-DD'), 'ACCEPTED', 6, 1);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(13, TO_TIMESTAMP('2020-03-20', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-25', 'YYYY-MM-DD'), 'ACCEPTED', 1, 2);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(14, TO_TIMESTAMP('2020-03-25', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-30', 'YYYY-MM-DD'), 'ACCEPTED', 2, 3);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(15, TO_TIMESTAMP('2020-03-17', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-20', 'YYYY-MM-DD'), 'REJECTED', 3, 4);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(16, TO_TIMESTAMP('2020-03-28', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-30', 'YYYY-MM-DD'), 'PENDING', 4, 5);
insert into reservation (id, check_in, check_out, state, room_id, client_id) values
(17, TO_TIMESTAMP('2020-03-25', 'YYYY-MM-DD'), TO_TIMESTAMP('2020-03-29', 'YYYY-MM-DD'), 'PENDING', 5, 6);*/

DROP TABLE ROOM_RESERVATIONS;