package cz.vse.hotelsemestral.logic;

import cz.vse.hotelsemestral.entity.Room;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

public class Methods {

    public static Date parseStringToDate(String input){

        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        if (!input.equals("")) {
            try {
                date = formatter.parse(input);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return date;
    }

    public static String parseDateToString (Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    public static  ArrayList<Room> removeDuplicates(ArrayList<Room> list)
    {
        Set<Room> set = new LinkedHashSet<>();
        set.addAll(list);
        list.clear();
        list.addAll(set);
        return list;
    }

    public static String formatDate(String date){
        String [] tokens = date.split("-");
        StringBuilder output = new StringBuilder("");
        output.append(tokens[2]).append(".").append(tokens[1]).append(".").append(tokens[0]);
        return output.toString();
     }

}
