package cz.vse.hotelsemestral.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
public class MainPageController {

    @GetMapping("/main")
    public String mainPage(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model, Principal principal) {

        if (principal != null && principal.getName().equals("admin")) {
            model.addAttribute("principal", principal.getName());
        } else if (principal != null) {
            model.addAttribute("principal", "user");
        } else {
            model.addAttribute("principal", false);
        }
        model.addAttribute("name", name);
        return "main";
    }

}
