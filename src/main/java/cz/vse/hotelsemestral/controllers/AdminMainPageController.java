package cz.vse.hotelsemestral.controllers;

import cz.vse.hotelsemestral.entity.Client;
import cz.vse.hotelsemestral.entity.Reservation;
import cz.vse.hotelsemestral.logic.Methods;
import cz.vse.hotelsemestral.service.ClientService;
import cz.vse.hotelsemestral.service.ReservationService;
import cz.vse.hotelsemestral.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.List;

@Controller
public class AdminMainPageController {

    @Autowired
    ReservationService reservationService;

    @Autowired
    RoomService roomService;

    @Autowired
    ClientService clientService;


    @GetMapping("/admin-main")
    public String adminMainPage (Model model){


        List<Reservation> reservations = reservationService.getAllReservations();
        Collections.reverse(reservations);
        model.addAttribute("reservations", reservations);

        return "adminMain";
    }

    @GetMapping("/admin-reservation-detail")
    public String adminReservationDetailPage (@RequestParam(name = "reservationId", required = true) Integer id, Model model){

        Reservation reservation = reservationService.getReservationById(id);
        Client client = clientService.findById(reservation.getClient().getId());
        model.addAttribute("client",client);
        model.addAttribute("dateOfBirthFormatted", Methods.formatDate(Methods.parseDateToString(client.getDateOfBirth())));
        model.addAttribute("room",roomService.getRoomByName(reservation.getRoom().getName()));
        model.addAttribute("checkInFormatted", Methods.parseDateToString(reservation.getCheckIn()));
        model.addAttribute("checkOutFormatted", Methods.parseDateToString(reservation.getCheckOut()));

        return "adminReservationDetail";
    }

    @PostMapping("/admin-main")
    public String adminMainPageUpdateReservationState(
            @RequestParam(name = "state", required = true) String state,
            @RequestParam(name = "reservationId", required = true) Integer id,
            Model model){

        Reservation reservation = reservationService.getReservationById(id);
        reservation.setState(state);
        reservationService.updateReservation(reservation);
        List<Reservation> reservations = reservationService.getAllReservations();
        Collections.reverse(reservations);
        model.addAttribute("reservations", reservations);
        return "adminMain";
    }
}
