package cz.vse.hotelsemestral.controllers;

import cz.vse.hotelsemestral.entity.Client;
import cz.vse.hotelsemestral.entity.Reservation;
import cz.vse.hotelsemestral.logic.Methods;
import cz.vse.hotelsemestral.service.ClientService;
import cz.vse.hotelsemestral.service.ReservationService;
import cz.vse.hotelsemestral.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
public class UserMainPageController {
    
    @Autowired
    ReservationService reservationService;
    @Autowired
    ClientService clientService;
    @Autowired
    RoomService roomService;

    @GetMapping("/user-main")
    public String userMainPage (Model model, Principal principal){

        Client client = clientService.getClientByEmail(principal.getName());
        List <Reservation> reservations = reservationService.getAllReservationsByClient(client.getId());
        model.addAttribute("reservations", reservations);
        return "userMain";
    }

    @GetMapping("/user-reservation-detail")
    public String userReservationDetailPage (@RequestParam(name = "reservationId", required = true) int id, Model model, Principal principal) {

        Client client = clientService.getClientByEmail(principal.getName());
        Reservation reservation = reservationService.getReservationById(id);

        model.addAttribute("reservationId", reservation.getId());
        model.addAttribute("client",client);
        model.addAttribute("dateOfBirthFormatted", Methods.formatDate(Methods.parseDateToString(client.getDateOfBirth())));
        model.addAttribute("room",roomService.getRoomByName(reservation.getRoom().getName()));
        model.addAttribute("checkInFormatted", Methods.parseDateToString(reservation.getCheckIn()));
        model.addAttribute("checkOutFormatted", Methods.parseDateToString(reservation.getCheckOut()));

        return "userReservationDetail";
    }
}
