package cz.vse.hotelsemestral.controllers;

import cz.vse.hotelsemestral.entity.Client;
import cz.vse.hotelsemestral.entity.Reservation;
import cz.vse.hotelsemestral.entity.Role;
import cz.vse.hotelsemestral.entity.User;
import cz.vse.hotelsemestral.logic.Methods;
import cz.vse.hotelsemestral.service.ClientService;
import cz.vse.hotelsemestral.service.ReservationService;
import cz.vse.hotelsemestral.service.RoomService;
import cz.vse.hotelsemestral.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ReservationAcceptedPageController {

    @Autowired
    ClientService clientService;
    @Autowired
    ReservationService reservationService;
    @Autowired
    RoomService roomService;
    @Autowired
    UserService userService;

    @GetMapping("/reservation-accepted")
    public String reservationAcceptedPage (@RequestParam(name="email", required=true) String  email,
                                           @RequestParam(name="name", required=true) String name,
                                           @RequestParam(name="dateOfBirth", required=true) String dateOfBirth,
                                           @RequestParam(name="number", required=true) String number,
                                           @RequestParam(name="roomName", required=true) String roomName,
                                           @RequestParam(name="checkIn", required=true) String checkIn,
                                           @RequestParam(name="checkOut", required=true) String checkOut,
                                           @RequestParam(name="createAccount", required=false) Boolean createAccount,
                                           @RequestParam(name="password", required = false) String password,
                                           Model model){

        if (createAccount == null){
            createAccount = false;
        }
        Client client = null;
        if (clientService.getClientByEmail(email) == null) {
            client = new Client(email, name, Methods.parseStringToDate(dateOfBirth), number);
            clientService.createClient(client);
        }
        if (client == null) {
            reservationService.createReservation(new Reservation(Methods.parseStringToDate(checkIn), Methods.parseStringToDate(checkOut),
                    roomService.getRoomByName(roomName), "PENDING", clientService.getClientByEmail(email)));
        } else {
            reservationService.createReservation(new Reservation(Methods.parseStringToDate(checkIn), Methods.parseStringToDate(checkOut),
                    roomService.getRoomByName(roomName), "PENDING", client));
        }
        if (createAccount) {
            if (userService.findByUsername(email) == null && client == null){
                userService.createUser(new User(password, email,clientService.getClientByEmail(email),true, Role.USER));
            } else if (userService.findByUsername(email) == null && client != null){
                userService.createUser(new User(password, email, client, true, Role.USER));
            }
        }

        return "reservationAccepted";
    }

}
