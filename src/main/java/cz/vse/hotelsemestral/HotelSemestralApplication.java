package cz.vse.hotelsemestral;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelSemestralApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelSemestralApplication.class, args);
	}

}
