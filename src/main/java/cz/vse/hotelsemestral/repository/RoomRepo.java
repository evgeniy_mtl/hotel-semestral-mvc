package cz.vse.hotelsemestral.repository;

import cz.vse.hotelsemestral.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface RoomRepo extends JpaRepository<Room, Integer> {

    @Query("SELECT room FROM Room room WHERE room.name = :name")
    Room getRoomByName(@Param("name")String name);

    @Query(value = "Select * From Room Where Room.Id Not In(SELECT room.id FROM room INNER JOIN reservation ON reservation.room_id = room.id WHERE (reservation.check_In <= :checkOut AND reservation.check_Out >= :checkIn AND reservation.state != 'REJECTED'))", nativeQuery = true)
    List <Room> getAvailableRooms (@Param("checkIn")Date checkIn, @Param("checkOut")Date checkOut);

}
