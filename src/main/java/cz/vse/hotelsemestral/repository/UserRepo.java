package cz.vse.hotelsemestral.repository;

import cz.vse.hotelsemestral.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository <User, Integer> {


    User findByUsername(String username);

}
