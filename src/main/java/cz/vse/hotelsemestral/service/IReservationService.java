package cz.vse.hotelsemestral.service;

import cz.vse.hotelsemestral.entity.Reservation;
import cz.vse.hotelsemestral.entity.Room;

import java.util.List;

public interface IReservationService {

    List<Reservation> getAllReservations();

    List<Reservation> getAllReservationsByClient(final int id);

    void updateReservation(final Reservation reservation);

    void createReservation(final Reservation reservation);

    Reservation getReservationById(final int id);


}
