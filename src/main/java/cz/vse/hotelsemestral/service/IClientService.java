package cz.vse.hotelsemestral.service;

import cz.vse.hotelsemestral.entity.Client;
import cz.vse.hotelsemestral.entity.Reservation;

import java.util.List;

public interface IClientService {

    List<Client> getAllClients();

    void deleteClient (final int id);

    void updateClient(final Client client);

    void createClient(final Client client);

    Client getClientByEmail(String email);

    Client findById(int id);

}
