package cz.vse.hotelsemestral.service;

import cz.vse.hotelsemestral.entity.Client;
import cz.vse.hotelsemestral.entity.User;
import cz.vse.hotelsemestral.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {

    @Autowired
    UserRepo userRepo;


    @Override
    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public void createUser(User user) {
        userRepo.saveAndFlush(user);
    }
}
